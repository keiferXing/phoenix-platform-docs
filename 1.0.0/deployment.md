### 部署手册
	
#### 1.linux服务器安装unrar插件
	
远程下载 
	
	wget https://www.rarlab.com/rar/rarlinux-x64-5.5.0.tar.gz
	
[本地下载](attached/rarlinux-x64-5.5.0.tar.gz)

#### 2.服务器安装kotlin

**2.1 下载安装包** 
 
远程下载
	
	wget https://github.com/JetBrains/kotlin/releases/download/v1.2.41/kotlin-compiler-1.2.41.zip
	
[本地下载](attached/kotlin-compiler-1.2.41.zip)

**2.2 解压安装**
	
	unzip kotlin-compiler-1.2.41.zip
	vim /etc/profile
	export PATH=$PATH:/kotlinc/bin
	source /etc/profile
	kotlinc -version #验证安装

#### 3.部署解压服务
	nohup java -jar phoenix-services-XXXX.jar &
	注意： 线上部署需要制定jvm参数