# Change Log

## 1.0.1 / 2018-05-30 / KeiferXing
**Added**
- add1

**Changed**
- change1

**Fixed**
- bug1
- bug2
- bug3

**Deprecated**
- deprecated record xxxxx

## 1.0.0 / 2018-05-08 / KeiferXing
**Added**
- Initial implementation.
